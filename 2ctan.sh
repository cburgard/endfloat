#!/bin/sh
#
# 2011-12-12: 1st version for the endfloat package
# 2011-12-23: "*.cfg" changed to "efxmpl.cfg"
# 2018-03-24: Adapted to current version of ctanify
# 2020-07-29: ltxdoc.cfg removed from created archive
#
dist_dir="$(pwd)"
#
rm -fr /tmp/endfloat
mkdir /tmp/endfloat || exit
cp README COPYING endfloat.dtx endfloat.ins endfloat.pdf efxmpl.cfg endfloat.sty /tmp/endfloat
cd /tmp/endfloat || exit

if ctanify --noauto \
     README "COPYING=doc/latex/endfloat" \
     *.dtx *.ins *.pdf \
     --tdsonly "*.cfg" "*.cfg=tex/latex/endfloat" \
     --tdsonly "*.sty" "*.sty=tex/latex/endfloat"
then
#  ctanupload.pl -l -p -U uktug \
#    --contribution=endfloat \
#    --name "Axel Sommerfeldt" --email "axel.sommerfeldt@f-m.fm" \
#    --summary-file "$dist_dir/SUMMARY" \
#    --directory=/macros/latex/contrib/endfloat \
#    --DoNotAnnounce=0 \
#    --license=free --freeversion=gpl \
#    --file=endfloat.tar.gz
#
#  chmod 444 *.log
## cp *.log $dist_dir
#  mcp "*.log" $dist_dir/#1.LOG

  rm -f "$dist_dir/endfloat.tar.gz"
  cp -a endfloat.tar.gz "$dist_dir"
fi

